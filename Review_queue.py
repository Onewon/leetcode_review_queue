import queue
import redis

# db = redis.Redis(host='localhost', port=6379, decode_responses=True)
# pool = redis.ConnectionPool(host='localhost', port=6379,db=0, decode_responses=True)
# db = redis.Redis(connection_pool=pool)
# for r_index in waiting_to_review:
#     db.rpush("review_queue",r_index)

# while(db.llen("review_queue")!=0):
#     print(db.lpop("review_queue"))

class ReviewServer:
    '''
    rpush()
    lpop()
    => Queue
    '''
    def __init__(self):
        self.pool = redis.ConnectionPool(host='localhost', port=6379,db=0, decode_responses=True)
        self.db = redis.Redis(connection_pool=self.pool)
        self.keyname = "review_queue"
        self.tempList=[]
    def get(self):
        if(self.tempList):
            return "Process current task first."
        if(self.isEmpty()==False):
            temp = self.db.lpop(self.keyname)
            self.tempList.append(temp)
            return "Now do exercise with Leetcode "+str(temp)
        else:
            return "No available task."
    def current(self):
        if(self.tempList):
            return "Current exercise is Leetcode"+str(self.tempList[0])
        else:
            return "Currently no review task."
    def done(self):
        if(self.tempList):
            temp = str(self.tempList[0])
            self.tempList.clear()
            return F"Pass LeetCode {temp}."
        else:
            return "No processing review task."
    def fail(self):
        copys = list()
        if(self.tempList):
            review_index = self.tempList[0]
            self.tempList.clear()
            while(self.db.llen(self.keyname)!=0):
                copys.append(int(self.db.lpop(self.keyname)))
            if(len(copys)<3):
                for index in copys:
                    self.db.rpush(self.keyname,index)
                self.db.rpush(self.keyname,review_index)
                return F"Fail LeetCode {review_index}, push into the tail of review queue."
            copy_left = copys[:2]
            copy_left.append(review_index)
            copy_right = copys[2:]
            _list = copy_left + copy_right
            for index in _list:
                self.db.rpush(self.keyname,index)
            return F"Fail LeetCode {review_index}, push into the tail of review queue."
        else:
            return ("No processing review task.")
    def insertList_1(self,new_tasklist):
        '''
        directly insert node
        '''
        for task in new_tasklist:
            self.db.rpush(self.keyname,task)
        pass
    def add(self,args):
        copys = list()
        bag = set()
        while(self.db.llen(self.keyname)!=0):
            copys.append(int(self.db.lpop(self.keyname)))
        for i in copys:
            bag.add(int(i))
        baglen=len(bag)
        bag.add(int(args))
        if(len(bag)==baglen):
            return "Item has exist."
        else:
            for index in copys:
                self.db.rpush(self.keyname,int(index))
            self.db.rpush(self.keyname,int(args))
            return F"Add Leetcode {args} into review queue."
    def isEmpty(self):
        length = self.db.llen(self.keyname)
        if(length==0):
            # print("There are no task in review queue.")
            return True
        else:
            return False
    def howmany(self):
        return (self.db.llen(self.keyname))
    def insertList(self,new_tasklist):
        copys = list()
        bag = set()
        while(self.db.llen(self.keyname)!=0):
            copys.append(int(self.db.lpop(self.keyname)))
        for i in copys:
            bag.add(int(i))
        for t in new_tasklist:
            baglen=len(bag)
            bag.add(t)
            if(len(bag)==baglen):
                print("Found one same.")
            else:
                copys.append(t)
        final = copys
        for task in final:
            self.db.rpush(self.keyname,task)
        print("Insert nodes Done.")
    def clear(self):
        while(self.db.llen(self.keyname)!=0):
            self.db.lpop(self.keyname)
        print("Review Queue is clear.")
    def display(self):
        backup = list()
        while(self.db.llen(self.keyname)!=0):
            backup.append(int(self.db.lpop(self.keyname)))
        for l in backup:
            self.db.rpush(self.keyname,l)
        return backup
    def test_add(self,args):
        self.db.lpush(self.keyname,int(args))
        return F"Add LeetCode {args} into the head of review queue."
# if __name__ == '__main__':
#     RS = ReviewServer()
