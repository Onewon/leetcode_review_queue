from flask import Flask
from Review_queue import ReviewServer
app = Flask(__name__)
rs = ReviewServer()
leetcode_index_waiting_add = [226,101,104,110,112,108,107,100,199,236]
before_list =[20,206,2,15,24,98,105]
# rs.clear()
# rs.insertList(leetcode_index_waiting_add)
# rs.insertList(before_list)

@app.route('/')
def index():
    return '<h1>Welcome Leetcode Review Queue!</h1>'
@app.route('/get/')
def get():
    return F'<h1>{rs.get()}</h1>\
    <div><form action="/done" method="get">\
    <input type="submit" value="Done" name="" id="done_btn" /></form>\
    </div>\
    <div><form action="/fail" method="get">\
    <input type="submit" value="Fail" name="" id="fail_btn" /></form>\
    </div>'
@app.route('/current/')
def current():
    return F'<h1>{rs.current()}</h1>\
    <div><form action="/done" method="get">\
    <input type="submit" value="Done" name="" id="done_btn" /></form>\
    </div>\
    <div><form action="/fail" method="get">\
    <input type="submit" value="Fail" name="" id="fail_btn" /></form>\
    </div>'
@app.route('/done/')
def done():
    return F"<h1>{rs.done()}</h1>"
@app.route('/fail/')
def fail():
    return F"<h1>{rs.fail()}</h1>"
@app.route('/count/')
def count():
    return F"<h1>There are {rs.howmany()} task(s) in review queue.</h1>"
@app.route('/show/')
def display():
    return F"<h1>The review queue {str(rs.display())}</h1>"
@app.route('/add/<int:leetcode_id>')
def addNode(leetcode_id):
    return F"<h1>{str(rs.add(leetcode_id))}</h1>"
@app.route('/addx/<int:leetcode_id>')
def test_addNode(leetcode_id):
    return F"<h1>{str(rs.test_add(leetcode_id))}</h1>"

if __name__ == '__main__':
    app.run(host="localhost", port=8080, debug=False)
'''
localhost/
localhost/get
localhost/current
localhost/done
localhost/fail
localhost/count
localhost/show
localhost/add/<index>
'''
# planA 2 button + rpushx [Done]
# planB redis bgsave (5 min)
'''=>
import time
import threading

def hello(s):
    print s
key = "xiaorui.cc"
for i in range(3):
    t = threading.Timer(3.0, hello,[key])
    t.start()
'''